package com.example.masteraaa.reproductor;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by MasterAAA on 09/05/2017.
 */
public class Adaptador extends ArrayAdapter {

    private Context contexto;
    private ArrayList<Cancion> datos;

    public Adaptador(Context c, ArrayList<Cancion> datos){

        super(c, R.layout.lista_hija,datos);
        contexto=c;
        this.datos=datos;
    }

    public View getView(int position, View convertView, ViewGroup parent){

        // metodo estatico que me va a devolver el layout inflado
        LayoutInflater inflador=LayoutInflater.from(contexto);
        // el metodo inflate de layoutinflater me devuelve la vista inflada
        convertView=inflador.inflate(R.layout.lista_hija, null);

        // accedo a traves de la posicion del array al objeto
        Cancion cancion=datos.get(position);

        ImageView imgCaratula=(ImageView)convertView.findViewById(R.id.imagenCaratula);
        imgCaratula.setImageResource(cancion.getCaratula());

        TextView tituloCancion=(TextView)convertView.findViewById(R.id.titulo);
        tituloCancion.setText(cancion.getTitulo());

        TextView autorCancion=(TextView)convertView.findViewById(R.id.autor);
        autorCancion.setText(cancion.getAutor());

        TextView duracionCancion=(TextView)convertView.findViewById(R.id.duracion);
        duracionCancion.setText(cancion.getDuracion());

//        TextView duracionCancion=(TextView)convertView.findViewById(R.id.duracion);
//        duracionCancion.setText((double) cancion.getDuracion());

//        TextView duracionCancion=(TextView)convertView.findViewById(R.id.duracion);

        // retorno la vista
        return convertView;
    }


}
