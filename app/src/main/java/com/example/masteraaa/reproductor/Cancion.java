package com.example.masteraaa.reproductor;

/**
 * Created by MasterAAA on 09/05/2017.
 */
public class Cancion {

    int caratula;
    String titulo, autor;
    String duracion;

    public Cancion(int caratula, String titulo, String autor, String duracion) {
        this.caratula = caratula;
        this.titulo = titulo;
        this.autor = autor;
        this.duracion = duracion;
    }

    public int getCaratula() {
        return caratula;
    }

    public void setCaratula(int caratula) {
        this.caratula = caratula;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getDuracion() {
        return duracion;
    }

    public void setDuracion(String duracion) {
        this.duracion = duracion;
    }
}
