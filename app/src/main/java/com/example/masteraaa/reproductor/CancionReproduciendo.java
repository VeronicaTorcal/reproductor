package com.example.masteraaa.reproductor;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class CancionReproduciendo extends AppCompatActivity implements View.OnClickListener {

    ImageView imagenCatatula;
    TextView tituloCancion, autorCancion;
    Button btnReproducir;
    MediaPlayer cancion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cancion_reproduciendo);

        imagenCatatula=(ImageView)findViewById(R.id.imagenCaratula);
        tituloCancion=(TextView)findViewById(R.id.titulo);
        autorCancion=(TextView)findViewById(R.id.autor);
        btnReproducir=(Button)findViewById(R.id.botonPlay);

       // btnReproducir.setOnClickListener(this);
        //RECOJO LOS DATOS DEL BUNDLE
        Bundle datitos=getIntent().getExtras();
        String titulo=datitos.getString("TITULO");
        String autor=datitos.getString("AUTOR");
        String duracion=datitos.getString("DURACION");

        switch (autor){
            case "Queen":
                imagenCatatula.setImageResource(R.drawable.queen);
                break;
            case "Cindi Lauper":
                imagenCatatula.setImageResource(R.drawable.cindylauper);
                break;
            case "Bon Jovi":
                imagenCatatula.setImageResource(R.drawable.bonjovi);
                break;
            case "Madonna":
                imagenCatatula.setImageResource(R.drawable.madonna);
                break;
        }

        tituloCancion.setText(titulo);
        autorCancion.setText(autor);

        btnReproducir.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {

        if (autorCancion.getText().equals("Queen")){
           cancion=(MediaPlayer.create(this, R.raw.we_are_the_champions));
            cancion.start();
        }else if (autorCancion.getText().equals("Bon Jovi")){
            cancion=(MediaPlayer.create(this, R.raw.livin_on_a_prayer));
            cancion.start();
        } else if (autorCancion.getText().equals("Cindi Lauper")){
            cancion=(MediaPlayer.create(this, R.raw.girls_just_want_to_have_fun));
            cancion.start();
        } else{
            cancion=(MediaPlayer.create(this, R.raw.like_a_prayer));
            cancion.start();
        }

    }
}
