package com.example.masteraaa.reproductor;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    ListView listapadre;
    // creo el array de objetos cancion
    ArrayList<Cancion> datos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listapadre=(ListView)findViewById(R.id.listapadre);

        datos=new ArrayList<Cancion>();
        datos.add(new Cancion(R.drawable.bonjovi,"Livin on a Prayer","Bon Jovi","4.00"));
        datos.add(new Cancion(R.drawable.cindylauper,"Girls just to have fun","Cindy Lauper","4.01"));
        datos.add(new Cancion(R.drawable.madonna,"Like a prayer","Madonna","4.02"));
        datos.add(new Cancion(R.drawable.queen,"We are the champions","Queen","4.03"));

        //instancio el adaptador y lo aplico al listview
        Adaptador adaptador=new Adaptador(this, datos);
        listapadre.setAdapter(adaptador);

        // declaro e implemento un escuchador
        listapadre.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Intent i;
                String titulo="";
                String autor="";
                String duracion="";
                Bundle datitos;
                switch (position){
                    case 0:
                        titulo="Livin on a Prayer";
                        autor="Bon Jovi";
                        duracion="4.00";
                        i= new Intent(getApplicationContext(), CancionReproduciendo.class);
                        datitos=new Bundle();
                        datitos.putString("TITULO", titulo);
                        datitos.putString("AUTOR", autor);
                        datitos.putString("DURACION", duracion);
                        i.putExtras(datitos);
                        startActivity(i);

                       // Toast.makeText(getApplicationContext(),datos.get(position).getTitulo(),Toast.LENGTH_SHORT).show();
                        break;
                    case 1:
                        titulo="Girls just to have fun";
                        autor="Cindi Lauper";
                        duracion="4.01";
                        i= new Intent(getApplicationContext(), CancionReproduciendo.class);
                        datitos=new Bundle();
                        datitos.putString("TITULO", titulo);
                        datitos.putString("AUTOR", autor);
                        datitos.putString("DURACION", duracion);
                        i.putExtras(datitos);
                        startActivity(i);
                        //Toast.makeText(getApplicationContext(),datos.get(position).getTitulo(),Toast.LENGTH_SHORT).show();
                        break;
                    case 2:
                        titulo="Like a prayer";
                        autor="Madonna";
                        duracion="4.02";
                        i= new Intent(getApplicationContext(), CancionReproduciendo.class);
                        datitos=new Bundle();
                        datitos.putString("TITULO", titulo);
                        datitos.putString("AUTOR", autor);
                        datitos.putString("DURACION", duracion);
                        i.putExtras(datitos);
                        startActivity(i);
                       // Toast.makeText(getApplicationContext(),datos.get(position).getTitulo(),Toast.LENGTH_SHORT).show();
                        break;
                    case 3:
                        titulo="We are the champions";
                        autor="Queen";
                        duracion="4.03";
                        i= new Intent(getApplicationContext(), CancionReproduciendo.class);
                        datitos=new Bundle();
                        datitos.putString("TITULO", titulo);
                        datitos.putString("AUTOR", autor);
                        datitos.putString("DURACION", duracion);
                        i.putExtras(datitos);
                        startActivity(i);
                        //Toast.makeText(getApplicationContext(),datos.get(position).getTitulo(),Toast.LENGTH_SHORT).show();
                        break;


                }



            }
        });
    }
}
